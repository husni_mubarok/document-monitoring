<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{Config::get('constants.path.plugin')}}/images/favicon.png">
    <title>Document Monitoring | @yield('title')</title>
    <link href="{{Config::get('constants.path.bootstrap2')}}/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <!-- Menu CSS -->
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- vector map CSS -->
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{Config::get('constants.path.css')}}/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{Config::get('constants.path.css')}}/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{Config::get('constants.path.css')}}/colors/default.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.css">
    <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.css">

    <!-- jQuery -->

</head>

<body>
    @include('layouts.editor.header')
    @yield('content')
    @include('layouts.editor.footer')

    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery/dist/jquery.min.js"></script>

    <script src="{{Config::get('constants.path.bootstrap2')}}/dist/js/tether.min.js"></script>
    <script src="{{Config::get('constants.path.bootstrap2')}}/dist/js/bootstrap.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="{{Config::get('constants.path.js')}}/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{Config::get('constants.path.js')}}/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{Config::get('constants.path.js')}}/custom.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>


    @yield('js')


    @yield('customjs')

</body>

</html>
