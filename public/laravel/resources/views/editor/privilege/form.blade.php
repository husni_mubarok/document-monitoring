@extends('layouts.editor.template')
@section('content')
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-user"></i> Privilege
    <small>Auth</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Auth</a></li>
    <li class="active">Privilege</li>
  </ol>
</section> 
<section class="content">
	<div class="col-md-6 col-sm-6 col-xs-6"> 
		<section class="content box box-solid">
			<div class="row">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			    	<div class="col-md-1"></div>
			    	<div class="col-md-12">
				        <div class="x_panel">
			                <h2>
			                	@if(isset($user))
			                	<i class="fa fa-pencil"></i>
			                	@else
			                	<i class="fa fa-plus"></i>
			                	@endif
			                	&nbsp;Privilege
		                	</h2>
			                <hr>
				            <div class="x_content">
				                @include('errors.error')

				                @if(isset($user))
				                {!! Form::model($user, array('route' => ['editor.privilege.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
			                    @else
			                    {!! Form::open(array('route' => 'editor.privilege.store', 'files' => 'true'))!!}
			                    @endif
			                    {{ csrf_field() }}
			                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
			                    	{{ Form::label('user_id', 'Username') }}
			                    	@if(isset($user))
			                    	{{ Form::text('user_id', $user->username, ['class' => 'form-control', 'disabled' => 'true']) }}
			                    	@else
			                    	{{ Form::select('user_id', $username_list, old('user_id'), ['class' => 'form-control']) }}
			                    	@endif
			                    	<br>

			                    	<table class="table">
			                    	<thead>
			                    		<tr>
			                    			<th><i class="fa fa-gear"></i>|<i class="fa fa-wrench"></i></th>
			                    			@foreach($action_list as $action_key => $action)
			                    			<th>{{$action}}</th>
			                    			@endforeach
			                    		</tr>
			                    	</thead>
			                    	<tbody>
			                    		@foreach($module_list as $module_key => $module)
			                    		<tr>
			                    			<td>{{$module}}</td>
			                    			@foreach($action_list as $action_key => $action)
			                    			<td>{{ Form::checkbox('privilege['.$module_key.']['.$action_key.']', 1, null, ['id' => 'privilege_'.$module_key.'_'.$action_key]) }}</td>
			                    			@endforeach
			                    		</tr>
			                    		@endforeach
			                    	</tbody>
			                    	</table>

		                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
		                    	</div>
		                        {!! Form::close() !!}
				             </div>
				        </div>
			        </div>
			    </div>
			</div>
		</section>
	</div>
</section> 
@stop

@section('scripts')
@if(isset($user))
<script>
jQuery.each({!! $user->privilege !!}, function(key, value)
{
	$('#privilege_'+value['module_id']+'_'+value['action_id']).attr('checked', true);
});
</script>
@endif
@stop
