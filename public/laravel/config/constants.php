<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    |
    */

    'path' => [
        'uploads' => '/public/uploads',
        'bootstrap' => '/assets/laravel/bootstrap',
        'bootstrap2' => '/assets/bootstrap',
        'css' => '/assets/css',
        'scss' => '/assets/lte_sass/build/scss',
        'img' => '/assets/img',
        'js' => '/assets/js',
        'plugin' => '/assets/plugins',
    ],


    // husni
    // 'path' => [
    //     'uploads' => '/document-monitoring/public/uploads',
    //     'bootstrap' => '/document-monitoring/public/assets/laravel/bootstrap',
    //     'bootstrap2' => '/document-monitoring/public/assets/bootstrap',
    //     'css' => '/document-monitoring/public/assets/css',
    //     'scss' => '/document-monitoring/public/assets/lte_sass/build/scss',
    //     'img' => '/document-monitoring/public/assets/img',
    //     'js' => '/document-monitoring/public/assets/js',
    //     'plugin' => '/document-monitoring/public/assets/plugins',
    // ],
];
