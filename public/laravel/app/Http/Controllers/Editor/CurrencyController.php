<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\CurrencyRequest;
use App\Http\Controllers\Controller;
use App\Model\Currency; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;

class CurrencyController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'currencyname' => 'required|min:2'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $currencys = Currency::all();
    return view ('editor.currency.index', compact('currencys'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Currency::orderBy('currency_name', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->currency_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Currency(); 
    $post->currency_name = $request->currencyname; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    $post = new Userlog(); 
    $post->description = '<b>Created Currency Data</b><br> Name: '. $request->currencyname . ', <br>Description: ' . $request->description;  
    $post->user_id = Auth::id();
    $post->date_time = Carbon::now();
    $post->type = 'Container';
    $post->save();

    $post = DB::insert("UPDATE user SET read_notif = 1");

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $currency = Currency::Find($id);
    echo json_encode($currency); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

    //log
    $olddata = Currency::where('id', $id)->first(); 

    $post = new Userlog(); 
    $post->description = '<b>Update Currency Data</b><br> Name: '. $olddata->currency_name . ' to: '. $request->currencyname . ', <br>Description: ' . $olddata->description. ' to: ' . $request->description; 
    $post->user_id = Auth::id();
    $post->date_time = Carbon::now();
    $post->type = 'Currency';
    $post->save();

    $post = DB::insert("UPDATE user SET read_notif = 1");

    //data
    $post = Currency::Find($id); 
    $post->currency_name = $request->currencyname; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Currency::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Currency::where('id', $id["1"])->get();
    $post = Currency::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
