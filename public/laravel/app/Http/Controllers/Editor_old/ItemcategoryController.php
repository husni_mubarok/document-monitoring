<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemcategoryRequest;
use App\Http\Controllers\Controller;
use App\Model\Itemcategory; 
use Validator;
use Response;
use App\Post;
use View;

class ItemcategoryController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'itemcategoryname' => 'required|min:2'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $itemcategorys = Itemcategory::all();
    return view ('editor.itemcategory.index', compact('itemcategorys'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Itemcategory::orderBy('item_category_name', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->item_category_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };
       })

       ->addColumn('lsstatus', function ($itemdata) {
        if ($itemdata->ls_status == 1) {
          return '<span class="label label-success"><i class="fa fa-check"></i> </span>';
        }else{
         return '<span class="label label-danger"><i class="fa fa-close"></i> </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Itemcategory(); 
    $post->item_category_name = $request->itemcategoryname; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->ls_status = $request->ls_status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $itemcategory = Itemcategory::Find($id);
    echo json_encode($itemcategory); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Itemcategory::Find($id); 
    $post->item_category_name = $request->itemcategoryname; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->ls_status = $request->ls_status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Itemcategory::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Itemcategory::where('id', $id["1"])->get();
    $post = Itemcategory::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
