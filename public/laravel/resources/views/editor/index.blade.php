@extends('layouts.editor.template')
@section('title', 'Dashboard') 
@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Dashboard 1</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                <ol class="breadcrumb">
                    <li><a href="index.html">Dashboard</a></li>
                    <li class="active">Dashboard 1</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="white-box">
                    <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small> Site Traffic</h3>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Overall Growth</h6> <b>80.40%</b></div>
                        <div class="stat-item">
                            <h6>Montly</h6> <b>15.40%</b></div>
                        <div class="stat-item">
                            <h6>Day</h6> <b>5.50%</b></div>
                    </div>
                    <div id="sparkline8"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="white-box">
                    <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>Site Traffic</h3>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Overall Growth</h6> <b>80.40%</b></div>
                        <div class="stat-item">
                            <h6>Montly</h6> <b>15.40%</b></div>
                        <div class="stat-item">
                            <h6>Day</h6> <b>5.50%</b></div>
                    </div>
                    <div id="sparkline9"></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="white-box">
                    <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Site Traffic</h3>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Overall Growth</h6> <b>80.40%</b></div>
                        <div class="stat-item">
                            <h6>Montly</h6> <b>15.40%</b></div>
                        <div class="stat-item">
                            <h6>Day</h6> <b>5.50%</b></div>
                    </div>
                    <div id="sparkline10"></div>
                </div>
            </div>
        </div> 
        <!--row -->
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="white-box">
                    <h3 class="box-title">Browser Stats</h3>
                    <ul class="basic-list">
                        <li>Google Chrome <span class="pull-right label-danger label">21.8%</span></li>
                        <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
                        <li>Apple Safari <span class="pull-right label-success label">21.8%</span></li>
                        <li>Internet Explorer <span class="pull-right label-info label">21.8%</span></li>
                        <li>Opera mini <span class="pull-right label-warning label">21.8%</span></li>
                        <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="white-box">
                    <h3 class="box-title">CPU LOAD</h3>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Usage</h6> <b>60GB</b></div>
                        <div class="stat-item">
                            <h6>Space</h6> <b>320 GB</b></div>
                        <div class="stat-item">
                            <h6>CPU</h6> <b>50%</b></div>
                    </div>
                    <div style="height: 280px;">
                        <div id="placeholder" class="demo-placeholder"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="white-box">
                    <div class="user-bg"> <img src="{{Config::get('constants.path.plugin')}}/images/large/img1.jpg" alt="user" style="100%">
                        <div class="overlay-box">
                            <div class="user-content">
                                <a href="javascript:void(0)"><img alt="img" class="thumb-lg img-circle" src="{{Config::get('constants.path.plugin')}}/images/users/genu.jpg"></a>
                                <h4 class="text-white">User Name</h4>
                                <h5 class="text-white">info@myadmin.com</h5>
                            </div>
                        </div>
                    </div>
                    <div class="user-btm-box">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-purple"><i class="ti-facebook"></i></p>
                                <h1>258</h1>
                            </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-blue"><i class="ti-twitter"></i></p>
                                <h1>125</h1>
                            </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <p class="text-danger"><i class="ti-dribbble"></i></p>
                                <h1>556</h1>
                            </div>
                            <div class="stats-row col-md-12 m-t-20 m-b-0 text-center">
                                <div class="stat-item">
                                    <h6>Contact info</h6> <b><i class="ti-mobile"></i> 123-456-7890</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        <!-- /.row -->
            <!-- .right-sidebar -->
            <div class="right-sidebar">
                <div class="slimscrollright">
                    <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                    <div class="r-panel-body">
                        <ul>
                            <li><b>Layout Options</b></li>
                            <li>
                                <div class="checkbox checkbox-info">
                                    <input id="checkbox1" type="checkbox" class="fxhdr">
                                    <label for="checkbox1"> Fix Header </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox checkbox-warning">
                                    <input id="checkbox2" type="checkbox" class="fxsdr">
                                    <label for="checkbox2"> Fix Sidebar </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox checkbox-success">
                                    <input id="checkbox4" type="checkbox" class="open-close">
                                    <label for="checkbox4"> Toggle Sidebar </label>
                                </div>
                            </li>
                        </ul>
                        <ul id="themecolors" class="m-t-20">
                            <li><b>With Light sidebar</b></li>
                            <li><a href="javascript:void(0)" theme="default" class="default-theme working">1</a></li>
                            <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                            <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                            <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                            <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                            <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                            <li><b>With Dark sidebar</b></li>
                            <br/>
                            <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                            <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                            <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                            <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                            <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                            <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                        </ul>
                        <ul class="m-t-20 chatonline">
                            <li><b>Chat option</b></li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><img src="{{Config::get('constants.path.plugin')}}/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.right-sidebar -->
        </div>
        <!-- /.container-fluid -->
    </div>
@stop


@section('scripts') 
<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
<script src="{{Config::get('constants.path.js')}}/dashboard2.js"></script>
@stop