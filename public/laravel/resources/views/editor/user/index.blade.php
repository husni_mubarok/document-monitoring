@extends('layouts.editor.master')

<style type="text/css">
	.data-table-pagination {
		margin-top: 20px;
		border-top: 2px solid #eee;
		padding-top: 20px;
	}
	.vuetable-pagination-info {
		font-size: 1.8rem;
		padding-bottom: 10px;
	}

  .text-padding-behavior{
    padding: 5px !important;
    margin-top: 20px !important;
  }
	/* Loading Animation: */
        .data-table {
            opacity: 1;
            position: relative;
            filter: alpha(opacity=100); /* IE8 and earlier */
        }
        .data-table.loading {
          opacity:0.5;
           transition: opacity .3s ease-in-out;
           -moz-transition: opacity .3s ease-in-out;
           -webkit-transition: opacity .3s ease-in-out;
        }
        .data-table.loading:after {
          position: absolute;
          content: '';
          top: 40%;
          left: 50%;
          margin: -30px 0 0 -30px;
          border-radius: 100%;
          -webkit-animation-fill-mode: both;
                  animation-fill-mode: both;
          border: 4px solid #42A5F5;
          height: 60px;
          width: 60px;
          background: transparent !important;
          display: inline-block;
          -webkit-animation: pulse 1s 0s ease-in-out infinite;
                  animation: pulse 1s 0s ease-in-out infinite;
        }
        @keyframes pulse {
          0% {
            -webkit-transform: scale(0.6);
                    transform: scale(0.6); }
          50% {
            -webkit-transform: scale(1);
                    transform: scale(1);
                 border-width: 12px; }
          100% {
            -webkit-transform: scale(0.6);
                    transform: scale(0.6); }
        }
</style>

@section('content')
<!-- Content Header (Page header) -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">User</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                  <ol class="breadcrumb">
                    <li><a href="#">Auth</a></li>
                    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">User</li>
                  </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <!-- <div class="table-responsive"> -->
                  <div class="col-sm-12">
                      <div class="white-box" id="app">

                        <div class="row">
                          <div class="col-sm-8 col-md-8 col-lg-8">
                            <div class="button-box">
                              <a href="{{ URL::route('editor.user.create') }}" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</a>
                              <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
                              <button @click="refreshTable()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>                            </div>
                          </div>
                          <div class="col-sm-4 col-md-4 col-lg-4 ">
                            <div class="btn-group">
                              <input type="text" class="form-control" v-model="searchFor" placeholder="search" @keyup.enter="setFilter"/>
                              <button class="btn btn-warning btn-flat" @click="resetFilter"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                          </div>
                        </div>




                          <hr>
                          <!-- <div class="table-responsive"> -->

                            <div class="box-body">
                                <div class="panel" >

                                    <div :class="[{'data-table': true}, loading]">
                                      <vuetable
                                        ref="vuetable"
                                        api-url="{{ URL::route('api.user.data') }}"
                                        :fields="columns"
                                        :append-params="moreParams"
                                        pagination-path=""
                                        :sort-order="sortOrder"
                                        :per-page="perPage"
                                        detail-row-component=""
                                        detail-row-transition="expand"
                                        track-by="user_id"
                                        @vuetable:pagination-data="onPaginationData"
                                        @vuetable:loading="showLoader"
                              					@vuetable:loaded="hideLoader"
                                        :css="css.table"
                                        >
                                        <template slot="actions" scope="props">
                                          <div class="btn-group">
                                              <button class="btn bg-warning btn-flat" @click="editRow(props.rowData)">Edit</button>
                                          </div>
                                        </template>

                                      </vuetable>

                                      <div class="data-table-pagination">
                                				<vuetable-pagination-info ref="paginationInfo"
                                					:info-template="paginationInfoTemplate">
                                				</vuetable-pagination-info>
                                				<vuetable-pagination ref="pagination"
                                					@vuetable-pagination:change-page="onChangePage"
                                					:css="css.pagination">
                                				</vuetable-pagination>
                                			</div>

                                </div>
                            </div>
                          </div>
                      <!-- </div> -->
                  </div>
              </div>
            <!-- </div> -->
          </div>
      </div>
    </div>
</div>
@stop


@section('js')

@stop


@section('customjs')

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"></script>
<script src="https://unpkg.com/vuetable-2@1.6.0"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>

  Vue.use(Vuetable)
  var vm = new Vue({

    el: '#app',
    data: {
      loading: '',
      searchFor: '',
      columns: [
        {
          name: 'user_id',
          title: '#',
          sortField: 'user_id'
        },

        {
          name: 'first_name',
          title: 'First Name',
        },
        {
          name: 'last_name',
          title: 'Last Name',
        },
        {
          name: 'username',
          title: 'Username',
        },
        {
          name: 'email',
          title: 'Email',
        },
        {
          name: 'register.date',
          title: 'Register Date',
          callback: 'onFormatDate|D MMMM YYYY'
        },
        {
          name: 'status',
          title:'Status',
          dataClass: 'badge badge-success badge-pill text-padding-behavior'
        },
        {
          name: '__slot:actions',
          title: 'Option'
        },
      ],
      moreParams: {},
      sortOrder: [{
  			field: 'user_id',
  			direction: 'desc'
  		}],
      css: {
  			table: {
  				tableClass: 'table table-xxs',
  				ascendingIcon: 'fa fa-angle-up',
  				descendingIcon: 'fa fa-angle-down',
  		    },
        pagination: {
          wrapperClass: "btn-group",
          activeClass: "active",
          disabledClass: "disabled",
          pageClass: "btn btn-default",
          linkClass: "btn btn-default",
          icons: {
            first: "icon-chevron-left",
            prev: "icon-arrow-left32",
            next: "icon-arrow-right32",
            last: "icon-chevron-right"
          }
        }
      },

      perPage: 10,
      paginationInfoTemplate: '<strong>Showing record</strong> {from} to {to} from {total} item(s)',
    },

    methods:{

			editRow(rowData){
				swal(`Edit Data ${rowData.user_id}`)
				.then(value => {
					location.href="{{ URL::route('editor.user.index') }}"
				})
			},

      setFilter(filterText){

        this.moreParams = {
          'filter' : this.searchFor
        }

        this.$nextTick(function(){
          this.$refs.vuetable.refresh()
        })
      },

      resetFilter(){
        moreParams = {}
        this.searchFor = ''
        this.setFilter()
      },

      refreshTable(){
        this.$refs.vuetable.refresh()
      },

      showLoader(){
        this.loading = 'loading'
      },

      hideLoader (){
        this.loading = ''
      },

      onPaginationData (tablePagination) {
        this.$refs.paginationInfo.setPaginationData(tablePagination)
        this.$refs.pagination.setPaginationData(tablePagination)
      },
      onChangePage (page) {
        this.$refs.vuetable.changePage(page)
      },
      onInitialized (fields) {
        console.log('onInitialized', fields)
        this.vuetableFields = fields
      },

      onCellClicked (data, field, event) {
        console.log('cellClicked: ', field.name)
        this.$refs.vuetable.toggleDetailRow(data.user_id)
      },

      onFormatDate(value, formated){
          if(value == null) return ''
          formated = (typeof formated === undefined) ? 'D MMM YYYY' : formated
          return moment(value, 'YYYY-MM-DD').format(formated)
      },

      onDataReset () {
        console.log('onDataReset')
        this.$refs.paginationInfo.resetData()
        this.$refs.pagination.resetData()
      },


    }

  })

</script>
@stop
