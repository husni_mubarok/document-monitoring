<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\CustomerRequest;
use App\Http\Controllers\Controller;
use App\Model\Customer;  
use App\Model\CustomerType;  
use App\Model\City;  
use Validator;
use Response;
use App\Post;
use View;

class CustomerController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'customer_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 
    return view ('editor.customer.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                customer.id,
                customer.customer_code,
                customer.customer_name,
                customer.address,
                customer.phone,
                customer.email,
                customer.bank_account,
                customer.rek_number,
                customer.fax,
                customer.customer_type_id,
                customer_type.customer_type_name,
                customer.contact_person,
                customer.hp,
                customer.website,
                customer.city_id,
                city.city_name,
                customer.street,
                customer.description,
                customer.image,
                customer.`status` 
              FROM
                customer
                LEFT JOIN customer_type ON customer.customer_type_id = customer_type.id
                LEFT JOIN city ON customer.city_id = city.id
              WHERE
              customer.deleted_at IS NULL';
      $customerdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($customerdata) 

      ->addColumn('action', function ($customerdata) {
        return '<a href="customer/edit/'.$customerdata->id.'" title="Edit"> <i class="ti-pencil-alt"></i></a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$customerdata->id."', '".$customerdata->customer_code."', '".$customerdata->customer_name."'".')"> <i class="ti-trash"></i></a>';
      })

       ->addColumn('image', function ($itemdata) {
          if ($itemdata->image == null) {
            return '<a class="fancybox" rel="group" href="../uploads/placeholder/placeholder.png"><img src="../uploads/placeholder/placeholder.png" class="img-thumbnail img-responsive" /></a>';
          }else{
           return '<a class="fancybox" rel="group" href="../uploads/customer/'.$itemdata->image.'"><img src="../uploads/customer/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
         };
       })

      ->addColumn('mstatus', function ($customerdata) {
        if ($customerdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }


  public function create()
  {  
    $customer_type_list = CustomerType::all()->pluck('customer_type_name', 'id'); 
    $city_list = City::all()->pluck('city_name', 'id'); 

    return view ('editor.customer.form', compact('customer_type_list', 'city_list'));
  }

  public function store(Request $request)
  {  
    $customer = new Customer(); 
    $customer->customer_code = $request->input('customer_code');  
    $customer->customer_name = $request->input('customer_name');  
    $customer->bank_account = $request->input('bank_account');  
    $customer->rek_number = $request->input('rek_number');  
    $customer->customer_type_id = $request->input('customer_type_id');  
    $customer->contact_person = $request->input('contact_person');  
    $customer->phone = $request->input('phone');  
    $customer->hp = $request->input('hp'); 
    $customer->fax = $request->input('fax'); 
    $customer->email = $request->input('email'); 
    $customer->website = $request->input('website'); 
    $customer->address = $request->input('address'); 
    $customer->city_id = $request->input('city_id'); 
    $customer->street = $request->input('street');  
    $customer->created_by = Auth::id();
    $customer->save();

    if($request->image)
    {
    $customer = Customer::FindOrFail($customer->id);
    $original_directory = "uploads/customer/";

    if(!File::exists($original_directory))
      {
        File::makeDirectory($original_directory, $mode = 0777, true, true);
      }

      //$file_extension = $request->image->getClientOriginalExtension();
      $customer->image = Carbon::now()->format("d-m-Y h-i-s").str_replace(" ", "", $request->image->getClientOriginalName());
      $request->image->move($original_directory, $customer->image);

      $thumbnail_directory = $original_directory."thumbnail/";
      if(!File::exists($thumbnail_directory))
        {
         File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
       }
       $thumbnail = Image::make($original_directory.$customer->image);
       $thumbnail->fit(10,10)->save($thumbnail_directory.$customer->image);

       $customer->save(); 
     }

    return redirect()->action('Editor\CustomerController@index'); 
  }

  public function edit($id)
  {
    $customer = Customer::find($id); 
    $customer_type_list = CustomerType::all()->pluck('customer_type_name', 'id'); 
    $city_list = City::all()->pluck('city_name', 'id'); 

    return view ('editor.customer.form', compact('customer', 'customer_type_list', 'city_list'));
  }

  public function update($id, Request $request)
  {
     
    $customer = Customer::Find($id);  
    $customer->customer_code = $request->input('customer_code');  
    $customer->customer_name = $request->input('customer_name');  
    $customer->bank_account = $request->input('bank_account');  
    $customer->rek_number = $request->input('rek_number');  
    $customer->customer_type_id = $request->input('customer_type_id');  
    $customer->contact_person = $request->input('contact_person');  
    $customer->phone = $request->input('phone');  
    $customer->hp = $request->input('hp'); 
    $customer->fax = $request->input('fax'); 
    $customer->email = $request->input('email'); 
    $customer->website = $request->input('website'); 
    $customer->address = $request->input('address'); 
    $customer->city_id = $request->input('city_id'); 
    $customer->street = $request->input('street');  
    $customer->updated_by = Auth::id();
    $customer->save();

    if($request->image)
    {
    $customer = Customer::FindOrFail($customer->id);
    $original_directory = "uploads/customer/";

    if(!File::exists($original_directory))
      {
        File::makeDirectory($original_directory, $mode = 0777, true, true);
      }

      //$file_extension = $request->image->getClientOriginalExtension();
      $customer->image = Carbon::now()->format("d-m-Y h-i-s").str_replace(" ", "", $request->image->getClientOriginalName());
      $request->image->move($original_directory, $customer->image);

      $thumbnail_directory = $original_directory."thumbnail/";
      if(!File::exists($thumbnail_directory))
        {
         File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
       }
       $thumbnail = Image::make($original_directory.$customer->image);
       $thumbnail->fit(10,10)->save($thumbnail_directory.$customer->image);

       $customer->save(); 
     }

    return redirect()->action('Editor\CustomerController@index'); 
  }

  public function delete($id)
  {
    $post =  Customer::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

}
