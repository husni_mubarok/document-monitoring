@extends('layouts.editor.template')
@if(isset($customer))
@section('title', 'Edit Customer') 
@else
@section('title', 'Add New Customer') 
@endif
@section('content')
 <!-- Page Content -->
<div id="page-wrapper">
  <div class="container-fluid">
      <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <h4 class="page-title">@if(isset($customer)) Edit @else Add New @endif Customer</h4>
          </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
              <ol class="breadcrumb">
                  <li><a href="{{ url('/editor') }}">Dashboard</a></li>
                  <li class="active">@if(isset($customer)) Edit @else Add New @endif Customer</li> 
              </ol>
          </div>
          <!-- /.col-lg-12 -->
      </div>
      <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        @if(isset($customer))
                        {!! Form::model($customer, array('route' => ['editor.customer.update', $customer->id], 'method' => 'PUT', 'files' => 'true'))!!}
                        @else
                        {!! Form::open(array('route' => 'editor.customer.store', 'files' => 'true'))!!}
                        @endif
                        {{ csrf_field() }}
                            <div class="form-body"> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('customer_code', 'Code *', ['class' => 'control-label']) }}
                                            {{ Form::text('customer_code', old('customer_code'), ['class' => 'form-control', 'placeholder' => 'Customer Code', 'id' => 'customer_code']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('customer_name', 'Name *', ['class' => 'control-label']) }}
                                            {{ Form::text('customer_name', old('customer_name'), ['class' => 'form-control', 'placeholder' => 'Customer Name', 'id' => 'customer_name']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('bank_account', 'Bank Account', ['class' => 'control-label']) }}
                                            {{ Form::text('bank_account', old('bank_account'), ['class' => 'form-control', 'placeholder' => 'Bank Account', 'id' => 'bank_account']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('rek_number', 'Rek Number', ['class' => 'control-label']) }}
                                            {{ Form::text('rek_number', old('rek_number'), ['class' => 'form-control', 'placeholder' => 'Rek Number', 'id' => 'rek_number']) }}
                                          </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('customer_type_id', 'Customer Type', ['class' => 'control-label']) }}
                                            {{ Form::select('customer_type_id', $customer_type_list, old('customer_type_id'), array('class' => 'form-control', 'placeholder' => 'Select Customer Type', 'id' => 'customer_type_id')) }} 
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('image', 'Photo', ['class' => 'control-label']) }}
                                            {{ Form::file('image', old('image'), ['class' => 'form-control', 'placeholder' => 'Photo', 'id' => 'image']) }}
                                          </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <h3 class="box-title m-t-40">Contact</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('contact_person', 'Contact Person', ['class' => 'control-label']) }}
                                            {{ Form::text('contact_person', old('contact_person'), ['class' => 'form-control', 'placeholder' => 'Contact Person', 'id' => 'contact_person']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('phone', 'Phone', ['class' => 'control-label']) }}
                                            {{ Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => 'Phone', 'id' => 'phone']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('hp', 'HP', ['class' => 'control-label']) }}
                                            {{ Form::text('hp', old('hp'), ['class' => 'form-control', 'placeholder' => 'HP', 'id' => 'hp']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('fax', 'Fax', ['class' => 'control-label']) }}
                                            {{ Form::text('fax', old('fax'), ['class' => 'form-control', 'placeholder' => 'Fax', 'id' => 'fax']) }} 
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                                            {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'id' => 'email']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                          {{ Form::label('website', 'Website', ['class' => 'control-label']) }}
                                          {{ Form::text('website', old('website'), ['class' => 'form-control', 'placeholder' => 'Website', 'id' => 'website']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                  <div class="col-md-12">
                                      <div class="form-group"> 
                                          {{ Form::label('address', 'Address', ['class' => 'control-label']) }}
                                          {{ Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => 'Address', 'id' => 'address']) }}
                                      </div>
                                  </div>
                                  <!--/span--> 
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('city_id', 'City', ['class' => 'control-label']) }}
                                            {{ Form::select('city_id', $city_list, old('city_id'), array('class' => 'form-control', 'placeholder' => 'Select City', 'id' => 'city_id')) }} 
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group"> 
                                            {{ Form::label('street', 'Street', ['class' => 'control-label']) }}
                                            {{ Form::text('street', old('street'), ['class' => 'form-control', 'placeholder' => 'Street', 'id' => 'street']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row--> 
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                <a href="{{ URL::route('editor.customer.index') }}" type="button" class="btn btn-default">Cancel</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@stop
@section('scripts')
 <!-- Add fancyBox -->
 <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
 <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
 <script type="text/javascript">
  $(document).ready(function() {
    $(".fancybox").fancybox();
  });
 </script>
@stop
