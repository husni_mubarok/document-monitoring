<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PibRequest;
use App\Http\Controllers\Controller;
use App\Model\Pib; 
use App\Model\Container; 
use Validator;
use Response;
use App\Post;
use View;

class PibController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'no_pib' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $container_list = Container::all();
    $sql = 'SELECT
              pib.id,
              pib.container_id,
              pib.no_pib,
              FORMAT(pib.cost_pib,0) AS cost_pib,
              pib.description,
              pib.`status`,
              pib.created_by,
              pib.updated_by,
              pib.deleted_by,
              pib.created_at,
              pib.updated_at,
              pib.deleted_at,
              CONCAT(container.prefix, container.container_no) AS containerno
            FROM
              pib
            LEFT JOIN container ON pib.container_id = container.id
            WHERE
            pib.deleted_at IS NULL';
    $pibs = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.pib.index', compact('pibs', 'container_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                  pib.id,
                  pib.container_id,
                  pib.no_pib,
                  FORMAT(pib.cost_pib,0) AS cost_pib,
                  pib.emkl,
                  pib.description,
                  DATE_FORMAT(pib.ata, "%d-%m-%Y") AS ata,
                  DATE_FORMAT(pib.original_doc, "%d-%m-%Y") AS original_doc,
                  DATE_FORMAT(pib.pajak_pib, "%d-%m-%Y") AS pajak_pib,
                  DATE_FORMAT(pib.kt2, "%d-%m-%Y") AS kt2,
                  DATE_FORMAT(pib.inspect_kt9, "%d-%m-%Y") AS inspect_kt9,
                  DATE_FORMAT(pib.tlg_respon, "%d-%m-%Y") AS tlg_respon,
                  pib.status_respon,
                  DATE_FORMAT(pib.delivery_date, "%d-%m-%Y") AS delivery_date,
                  pib.delivery_time,
                  pib.remarks, 
                  pib.`status`,
                  pib.created_by,
                  pib.updated_by,
                  pib.deleted_by,
                  pib.created_at,
                  pib.updated_at,
                  pib.deleted_at,
                  CONCAT(container.prefix, container.container_no) AS container_no
                FROM
                  pib
                LEFT JOIN container ON pib.container_id = container.id
                WHERE
                pib.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->no_pib."', '".$itemdata->cost_pib."'".')"> Delete</a>';
      })
 
      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Pib(); 
    $post->container_id = $request->container_id; 
    $post->no_pib = $request->no_pib; 
    $post->cost_pib = str_replace(",", "", $request->cost_pib); 
    $post->emkl = $request->emkl; 
    $post->description = $request->description; 
    $post->ata = $request->ata;
    $post->original_doc = $request->original_doc;
    $post->pajak_pib = $request->pajak_pib;
    $post->kt2 = $request->kt2;
    $post->inspect_kt9 = $request->inspect_kt9;
    $post->tlg_respon = $request->tlg_respon;
    $post->status_respon = $request->status_respon;
    $post->delivery_date = $request->delivery_date;
    $post->delivery_time = $request->delivery_time;
    $post->remarks = $request->remarks;
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $pib = Pib::Find($id);
     $sql = 'SELECT
                  pib.id,
                  pib.container_id,
                  pib.no_pib,
                  FORMAT(pib.cost_pib,0) AS cost_pib,
                  pib.description,
                  pib.emkl,
                  pib.ata,
                  pib.original_doc,
                  pib.pajak_pib,
                  pib.kt2,
                  pib.inspect_kt9,
                  pib.tlg_respon,
                  pib.status_respon,
                  pib.delivery_date,
                  pib.delivery_time,
                  pib.remarks, 
                  pib.`status`,
                  pib.created_by,
                  pib.updated_by,
                  pib.deleted_by,
                  pib.created_at,
                  pib.updated_at,
                  pib.deleted_at,
                  container.container_no
                FROM
                  pib
                LEFT JOIN container ON pib.container_id = container.id
                WHERE
                container.deleted_at IS NULL';
      $pib = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($pib); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Pib::Find($id); 
    $post->container_id = $request->container_id; 
    $post->no_pib = $request->no_pib; 
    $post->cost_pib = str_replace(",", "", $request->cost_pib); 
    $post->emkl = $request->emkl; 
    $post->description = $request->description; 
    $post->ata = $request->ata;
    $post->original_doc = $request->original_doc;
    $post->pajak_pib = $request->pajak_pib;
    $post->kt2 = $request->kt2;
    $post->inspect_kt9 = $request->inspect_kt9;
    $post->tlg_respon = $request->tlg_respon;
    $post->status_respon = $request->status_respon;
    $post->delivery_date = $request->delivery_date;
    $post->delivery_time = $request->delivery_time;
    $post->remarks = $request->remarks;
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Pib::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Pib::where('id', $id["1"])->get();
    $post = Pib::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}

public function generate(Request $request)
  {
     $user_id = Auth::id();
     $post = DB::insert("INSERT INTO pib (container_id) SELECT
                            container.id 
                          FROM
                            container 
                          WHERE
                            container.id NOT IN ( SELECT container_id FROM pib WHERE deleted_at IS NULL )");
     return response()->json($post);
  }

}
