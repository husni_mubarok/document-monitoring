<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PricelistRequest;
use App\Http\Controllers\Controller;
use App\Model\Pricelist; 
use App\Model\Pricelistbrand; 
use App\Model\Pricelistcategory; 
use App\Model\City; 
use Validator;
use Response;
use App\Post;
use View;

class PricelistController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'cityid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $city_list = City::all();

    return view ('editor.pricelist.index', compact('city_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                city.cityname,
                pricelist.cityid,
                pricelist.leadtime,
                pricelist.kgp,
                pricelist.kgs,
                FORMAT(pricelist.price,0) AS price,
                pricelist.`status`,
                pricelist.id 
              FROM
                pricelist
              LEFT JOIN city ON pricelist.cityid = city.id
              WHERE pricelist.deleted_at IS NULL';
      $pricelistdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($pricelistdata) 

      ->addColumn('action', function ($pricelistdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$pricelistdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$pricelistdata->id."', '".$pricelistdata->cityname."', '".$pricelistdata->cityname."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($pricelistdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$pricelistdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($pricelistdata) {
        if ($pricelistdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
    $post = new Pricelist(); 
    $post->cityid = $request->cityid; 
    $post->leadtime = $request->leadtime; 
    $post->kgp = $request->kgp;  
    $post->kgs = $request->kgs;  
    $post->price = $request->price; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
     $sql = 'SELECT
                city.cityname,
                pricelist.cityid,
                pricelist.leadtime,
                pricelist.kgp,
                pricelist.kgs,
                pricelist.price,
                pricelist.`status`,
                pricelist.id 
              FROM
                pricelist
              LEFT JOIN city ON pricelist.cityid = city.id';
      $pricelist = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($pricelist); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Pricelist::Find($id); 
    $post->cityid = $request->cityid; 
    $post->leadtime = $request->leadtime; 
    $post->kgp = $request->kgp;  
    $post->kgs = $request->kgs;  
    $post->price = $request->price; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    $post =  Pricelist::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    $post = Pricelist::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
