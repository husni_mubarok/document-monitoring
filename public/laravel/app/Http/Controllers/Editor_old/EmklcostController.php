<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\EmklcostRequest;
use App\Http\Controllers\Controller;
use App\Model\Emklcost; 
use App\Model\Emklcostlog; 
use Validator;
use Response;
use App\Post;
use View;

class EmklcostController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'emkl' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $itemdata = Emklcost::all();

    return view ('editor.emklcost.index', compact('emklcosts'));
  }

  public function indexlog()
  {
    $itemdata = Emklcost::all();

    return view ('editor.emklcost.indexlog', compact('emklcosts'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                emkl_cost.id,
                DATE_FORMAT(emkl_cost.datefrom, "%d-%m-%Y") AS datefrom,
                DATE_FORMAT(emkl_cost.dateto, "%d-%m-%Y") AS dateto,
                FORMAT(emkl_cost.emkl, 0) AS emkl,
                FORMAT(emkl_cost.`plugin`, 0) AS plugin,
                FORMAT(emkl_cost.ls, 0) AS ls,
                FORMAT(emkl_cost.kalog, 0) AS kalog,
                emkl_cost.status
              FROM
                emkl_cost';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 
      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }


  public function datalog(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                emkl_cost_log.id,
                DATE_FORMAT(emkl_cost_log.datefrom, "%d-%m-%Y") AS datefrom,
                DATE_FORMAT(emkl_cost_log.dateto, "%d-%m-%Y") AS dateto,
                FORMAT(emkl_cost_log.emkl, 0) AS emkl,
                FORMAT(emkl_cost_log.`plugin`, 0) AS plugin,
                FORMAT(emkl_cost_log.ls, 0) AS ls,
                FORMAT(emkl_cost_log.kalog, 0) AS kalog,
                DATE_FORMAT(emkl_cost_log.datefrom_to, "%d-%m-%Y") AS datefrom_to,
                DATE_FORMAT(emkl_cost_log.dateto_to, "%d-%m-%Y") AS dateto_to,
                FORMAT(emkl_cost_log.emkl_to, 0) AS emkl_to,
                FORMAT(emkl_cost_log.`plugin_to`, 0) AS plugin_to,
                FORMAT(emkl_cost_log.ls_to, 0) AS ls_to,
                FORMAT(emkl_cost_log.kalog_to, 0) AS kalog_to,
                emkl_cost_log.status,
                `user`.username,
                DATE_FORMAT(emkl_cost_log.updated_at, "%d-%m-%Y %H:%i") AS updated_at
              FROM
              emkl_cost_log
              JOIN `user`
              ON emkl_cost_log.updated_by = `user`.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 
      return Datatables::of($itemdata) 

      ->addColumn('datefrom_rs', function ($itemdata) {
        if($itemdata->datefrom == $itemdata->datefrom_to){
          return ''.$itemdata->datefrom.' to '.$itemdata->datefrom_to.'';
        }else{
          return '<font color="red">'.$itemdata->datefrom.' to '.$itemdata->datefrom_to.'</font>';
        }
      })

      ->addColumn('dateto_rs', function ($itemdata) {
        if($itemdata->dateto == $itemdata->dateto_to){
          return ''.$itemdata->dateto.' to '.$itemdata->dateto_to.'';
        }else{
          return '<font color="red">'.$itemdata->dateto.' to '.$itemdata->dateto_to.'</font>';
        }
      })

      ->addColumn('emkl_rs', function ($itemdata) {
        if($itemdata->emkl == $itemdata->emkl_to){
          return ''.$itemdata->emkl.' to '.$itemdata->emkl_to.'';
        }else{
          return '<font color="red">'.$itemdata->emkl.' to '.$itemdata->emkl_to.'</font>';
        }
      })

      ->addColumn('plugin_rs', function ($itemdata) {
        if($itemdata->plugin == $itemdata->plugin_to){
          return ''.$itemdata->plugin.' to '.$itemdata->plugin_to.'';
        }else{
          return '<font color="red">'.$itemdata->plugin.' to '.$itemdata->plugin_to.'</font>';
        }
      })

      ->addColumn('ls_rs', function ($itemdata) {
        if($itemdata->ls == $itemdata->ls_to){
          return ''.$itemdata->ls.' to '.$itemdata->ls_to.'';
        }else{
          return '<font color="red">'.$itemdata->ls.' to '.$itemdata->ls_to.'</font>';
        }
      })

      ->addColumn('kalog_rs', function ($itemdata) {
        if($itemdata->kalog == $itemdata->kalog_to){
          return ''.$itemdata->kalog.' to '.$itemdata->kalog_to.'';
        }else{
          return '<font color="red">'.$itemdata->kalog.' to '.$itemdata->kalog_to.'</font>';
        }
      })

      
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Emklcost(); 
    $post->currency_id = $request->currency_id; 
    $post->datefrom = $request->datefrom; 
    $post->dateto = $request->dateto; 
    $post->rate = str_replace(",", "", $request->rate); 
    $post->plusrate = str_replace(",", "", $request->plusrate); 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $emklcost = Emklcost::Find($id);
    $sql = 'SELECT
              emkl_cost.id,
              emkl_cost.datefrom,
              emkl_cost.dateto,
              FORMAT(emkl_cost.emkl, 0) AS emkl,
              FORMAT(emkl_cost.`plugin`, 0) AS plugin,
              FORMAT(emkl_cost.ls, 0) AS ls,
              FORMAT(emkl_cost.kalog, 0) AS kalog,
              emkl_cost.status
            FROM
              emkl_cost';
    $emklcost = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first();
    echo json_encode($emklcost); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

    $emkl = Emklcost::where('id', $id)->first(); 


    $post = new Emklcostlog(); 
    //old
    $post->datefrom = $emkl->datefrom; 
    $post->dateto = $emkl->dateto;  
    $post->emkl = $emkl->emkl;  
    $post->plugin = $emkl->plugin;  
    $post->ls = $emkl->ls;  
    $post->kalog = $emkl->kalog; 
    //new
    $post->datefrom_to = $request->datefrom; 
    $post->dateto_to = $request->dateto;  
    $post->emkl_to = str_replace(",", "", $request->emkl);  
    $post->plugin_to = str_replace(",", "", $request->plugin);  
    $post->ls_to = str_replace(",", "", $request->ls);  
    $post->kalog_to = str_replace(",", "", $request->kalog);  
    $post->updated_by = Auth::id();
    // $post->updated_at = Auth::id();
    $post->save();


    $post = Emklcost::Find($id); 
    $post->datefrom = $request->datefrom; 
    $post->dateto = $request->dateto; 
    $post->emkl = str_replace(",", "", $request->emkl); 
    $post->plugin = str_replace(",", "", $request->plugin); 
    $post->ls = str_replace(",", "", $request->ls); 
    $post->kalog = str_replace(",", "", $request->kalog);  
    $post->updated_by = Auth::id();
    $post->save();

    // $post = new Emklcostlog(); 
    // $post->datefrom = $request->datefrom; 
    // $post->dateto = $request->dateto;  
    // $post->updated_by = Auth::id();
    // // $post->updated_at = Auth::id();
    // $post->save();
    $post = DB::insert("UPDATE user SET read_notif = 1");


    return response()->json($post); 
  }
  }

    
}
