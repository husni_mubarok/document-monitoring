<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{Config::get('constants.path.plugin')}}/images/favicon.png">
    <title>Document Monitoring | @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{Config::get('constants.path.bootstrap2')}}/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

    <!-- Menu CSS -->
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- vector map CSS -->
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <link href="{{Config::get('constants.path.plugin')}}/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{Config::get('constants.path.css')}}/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{Config::get('constants.path.css')}}/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{Config::get('constants.path.css')}}/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{-- confirm --}}
    <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.css">
    <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.css">

    <!-- jQuery -->

</head>

<body>
    @include('layouts.editor.header')
    @yield('content')
    @include('layouts.editor.footer')

    <!-- jQuery -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- <script src="{{Config::get('constants.path.plugin')}}/bower_components/datatables/jquery.dataTables.min.js"></script> -->

    <!-- Bootstrap Core JavaScript -->

    <script src="{{Config::get('constants.path.bootstrap2')}}/dist/js/tether.min.js"></script>
    <script src="{{Config::get('constants.path.bootstrap2')}}/dist/js/bootstrap.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="{{Config::get('constants.path.js')}}/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{Config::get('constants.path.js')}}/waves.js"></script>
    <!-- Flot Charts JavaScript -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/flot/jquery.flot.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <!-- google maps api -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Sparkline charts -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- EASY PIE CHART JS -->
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery.easy-pie-chart/easy-pie-chart.init.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{Config::get('constants.path.js')}}/custom.min.js"></script>
    {{-- <script src="{{Config::get('constants.path.js')}}/dashboard2.js"></script>  --}}
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/jQueryConfirm/jquery-confirm.min.js"></script>
    {{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> --}}
    <!-- <script src="{{Config::get('constants.path.plugin')}}/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="{{Config::get('constants.path.plugin')}}/bower_components/datatables/jquery.dataTables.min.js"></script> -->

    @yield('init')

    <!-- <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
   </script> -->
</body>

</html>
