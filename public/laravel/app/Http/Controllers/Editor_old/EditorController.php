<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Model\Popup;


class EditorController extends Controller
{
    public function index()
    {
    	// $popup = Popup::all();

    	 $sql = 'SELECT
					popup.id,
					popup.popup_name,
					popup.description,
					popup.date_popup, 
					DATE_FORMAT(NOW(), "%Y-%m-%d") AS date_now 
				FROM
					popup';
    	$popup = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    	// dd($popup);

    	return view ('editor.index', compact('popup'));
    }

    public function notif()
    {

    	return view ('editor.notif.index');
    }
    
}
