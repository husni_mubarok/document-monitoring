@extends('layouts.editor.template')
@section('content')
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-user"></i> User
    <small>Auth</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Auth</a></li>
    <li class="active">User</li>
  </ol>
</section> 
<section class="content">
	<div class="col-md-6 col-sm-6 col-xs-6"> 
		<section class="content box box-solid">
			<div class="row">
			    <div class="col-md-12 col-sm-12 col-xs-12"> 
			        <div class="x_panel">
		                <h2>
		                	@if(isset($user))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	&nbsp;User
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

			                @if(isset($user))
			                {!! Form::model($user, array('route' => ['editor.user.update', $user->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.user.store', 'files' => 'true'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('username', 'Username') }}
		                    	@if(isset($user))
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control', 'disabled' => 'true']) }}
		                    	@else
		                    	{{ Form::text('username', old('username'), ['class' => 'form-control']) }}
		                    	@endif
		                    	<br> 

		                    	{{ Form::label('password', 'Password') }}
		                    	{{ Form::password('password', ['class' => 'form-control']) }}
		                    	<br>

		                    	{{ Form::label('password_confirmation', 'Confirm Password') }}
		                    	{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
		                    	<br>

	                            <button type="submit" class="btn btn-success pull-right btn-flat btn-lg"><i class="fa fa-check"></i> Save</button>
			                    <a href="{{ URL::route('editor.user.index') }}" class="btn btn-default pull-right btn-flat btn-lg" style="margin-right: 5px;"><i class="fa fa-close"></i> Close</a>
	                    	</div>
	                        {!! Form::close() !!}
			            </div> 
			        </div>
			    </div>
			</div>
		</section>
	</div>
</section> 
@stop
