@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-building"></i> Branch
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Branch</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
           <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <!-- <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button> -->
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th>Action</th> 
                  <th>Branch Name</th> 
                  <th>Description</th>
                  <th>Seq No</th>
                  <th>Field Name</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Branch Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">

            <div class="form-group">
              <label class="control-label col-md-3">Branch Name</label>
              <div class="col-md-8">
                <input name="branchname" id="branchname" class="form-control" type="text">
                <small class="errorBranchName hidden alert-danger"></small> 
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-8">
                <input name="description" id="description" class="form-control" type="text">
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Seq No</label>
              <div class="col-md-8">
                <input name="seq_no" id="seq_no" class="form-control" type="text">
              </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Field Name</label>
                <div class="col-md-8">
                  <input name="field_name" id="field_name" class="form-control" type="text" onkeyup="field_act();">
                </div>
              </div> 
            </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/branch/data') }}",
         columns: [  
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'branch_name', name: 'branch_name' },
         { data: 'description', name: 'description' },
         { data: 'seq_no', name: 'seq_no' },
         { data: 'field_name', name: 'field_name' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 
       function add()
      {
        document.getElementById("field_name").disabled = false;
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorBranchName').addClass('hidden');
        $('.errorBranchNo').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Branch'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.branch.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'branchname': $('#branchname').val(), 
            'description': $('#description').val(), 
            'seq_no': $('#seq_no').val(), 
            'field_name': $("#field_name").val().toLowerCase(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorBranchName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.branchname) {
                $('.errorBranchName').removeClass('hidden');
                $('.errorBranchName').text(data.errors.branchname);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.branch.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'branchname': $('#branchname').val(), 
          'seq_no': $('#seq_no').val(), 
          'description': $('#description').val(), 
          'field_name': $("#field_name").val().toLowerCase(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorBranchName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.branchname) {
                $('.errorBranchName').removeClass('hidden');
                $('.errorBranchName').text(data.errors.branchname);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorBranchName').addClass('hidden');
      $("#btnSave").attr("onclick","update("+id+")");
      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");
      document.getElementById("field_name").disabled = true;

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'branch/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="branchname"]').val(data.branch_name);
            $('[name="description"]').val(data.description);
            $('[name="field_name"]').val(data.field_name);
            $('[name="seq_no"]').val(data.seq_no);
            $('[name="status"]').val(data.status);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Branch'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'branch/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'branchname': $('#branchname').val(), 
            'description': $('#description').val(), 
            'seq_no': $('#seq_no').val(), 
            'field_name': $("#field_name").val().toLowerCase(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorBranchName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.branchname) {
                $('.errorBranchName').removeClass('hidden');
                $('.errorBranchName').text(data.errors.branchname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'branch/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'branchno': $('#branchno').val(), 
            'branchname': $('#branchname').val(), 
            'description': $('#description').val(), 
            'field_name': $("#field_name").val().toLowerCase(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
            $("#btnSave").attr("onclick","save()");
            $("#btnSaveAdd").attr("onclick","saveadd()");
          } 
        },
      })
      };

      function delete_id(id, branchname, field_name)
      {

        //var varnamre= $('#branchname').val();
        var branchname = branchname.bold();
        var field_name = field_name;

        $('[name="field_name"]').val(field_name);

        $.confirm({
          title: 'Confirm!',
          content: '<strong style ="color:red">Delete this data will automatically delete the ' + field_name + ' field on the capital goods!</strong><br/> Are you sure to delete ' + branchname + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'branch/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val(),
                'field_name': $("#field_name").val().toLowerCase(), 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
