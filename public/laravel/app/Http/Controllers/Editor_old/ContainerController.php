<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContainerRequest;
use App\Http\Controllers\Controller;
use App\Model\Container; 
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class ContainerController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'containername' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $containers = Container::all();
    return view ('editor.container.index', compact('containers'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Container::orderBy('container_name', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('concat', function ($itemdata) {
        return ''.$itemdata->prefix.''.$itemdata->container_no.'';
      })

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->container_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Container(); 
    $post->prefix = $request->prefix; 
    $post->container_no = $request->containerno; 
    $post->container_name = $request->containername; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $container = Container::Find($id);
    echo json_encode($container); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Container::Find($id); 
    $post->prefix = $request->prefix; 
    $post->container_no = $request->containerno; 
    $post->container_name = $request->containername; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    // foreach ($post->container_name as $notification) {
        echo $post->container_name;
    // };

    // $user = Container::find($id);

    // if (empty($post) === true) {
    //     return response('No user with the ID ' . $id . ' could be found');
    // }

    // try {
    //     $post->notify(new ToDb());
    // } catch (\Exception $e) {
    //     return response($e->getMessage());
    // }

    // return response('A DB notification has been logged for ' . $post->container_name);

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Container::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Container::where('id', $id["1"])->get();
    $post = Container::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
