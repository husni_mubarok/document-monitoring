<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'Editor'], function()
{
Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
});

//User Management
Route::group(['prefix' => 'editor', 'middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
	//Notif
	Route::get('/notif', ['as' => 'editor.notif.index', 'uses' => 'EditorController@notif']);
	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//edit
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);


	//User
		//index
	Route::get('/userbranch', ['middleware' => ['role:userbranch|read'], 'as' => 'editor.userbranch.index', 'uses' => 'UserbranchController@index']);
		//create
	Route::get('/userbranch/create', ['middleware' => ['role:userbranch|create'], 'as' => 'editor.userbranch.create', 'uses' => 'UserbranchController@create']);
	Route::post('/userbranch/create', ['middleware' => ['role:userbranch|create'], 'as' => 'editor.userbranch.store', 'uses' => 'UserbranchController@store']);
		//edit
	Route::get('/userbranch/{id}/edit', ['middleware' => ['role:userbranch|update'], 'as' => 'editor.userbranch.edit', 'uses' => 'UserbranchController@edit']);
	Route::put('/userbranch/{id}/edit', ['middleware' => ['role:userbranch|update'], 'as' => 'editor.userbranch.update', 'uses' => 'UserbranchController@update']);
		//delete
	Route::delete('/userbranch/{id}/delete', ['middleware' => ['role:userbranch|delete'], 'as' => 'editor.userbranch.delete', 'uses' => 'UserbranchController@delete']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);

	//Branch
		//index
	Route::get('/branch', ['middleware' => ['role:branch|read'], 'as' => 'editor.branch.index', 'uses' => 'BranchController@index']);
	Route::get('/branch/data', ['as' => 'editor.branch.data', 'uses' => 'BranchController@data']);
		//create
	Route::get('/branch/create', ['middleware' => ['role:branch|create'], 'as' => 'editor.branch.create', 'uses' => 'BranchController@create']);
	Route::post('/branch/create', ['middleware' => ['role:branch|create'], 'as' => 'editor.branch.store', 'uses' => 'BranchController@store']);
		//edit
	Route::get('/branch/edit/{id}', ['middleware' => ['role:branch|update'], 'as' => 'editor.branch.edit', 'uses' => 'BranchController@edit']);
	Route::put('/branch/edit/{id}', ['middleware' => ['role:branch|update'], 'as' => 'editor.branch.update', 'uses' => 'BranchController@update']);
		//delete
	Route::delete('/branch/delete/{id}', ['middleware' => ['role:branch|delete'], 'as' => 'editor.branch.delete', 'uses' => 'BranchController@delete']);
	Route::post('/branch/deletebulk', ['middleware' => ['role:branch|delete'], 'as' => 'editor.branch.deletebulk', 'uses' => 'BranchController@deletebulk']);

	//City
		//index
	Route::get('/city', ['middleware' => ['role:city|read'], 'as' => 'editor.city.index', 'uses' => 'CityController@index']);
	Route::get('/city/data', ['as' => 'editor.city.data', 'uses' => 'CityController@data']);
		//create
	Route::get('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.create', 'uses' => 'CityController@create']);
	Route::post('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.store', 'uses' => 'CityController@store']);
		//edit
	Route::get('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.edit', 'uses' => 'CityController@edit']);
	Route::put('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.update', 'uses' => 'CityController@update']);
		//delete
	Route::delete('/city/delete/{id}', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.delete', 'uses' => 'CityController@delete']);
	Route::post('/city/deletebulk', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.deletebulk', 'uses' => 'CityController@deletebulk']);


	//Customer
		//index
	Route::get('/customer', ['middleware' => ['role:customer|read'], 'as' => 'editor.customer.index', 'uses' => 'CustomerController@index']);
	Route::get('/customer/data', ['as' => 'editor.customer.data', 'uses' => 'CustomerController@data']);
		//create
	Route::get('/customer/create', ['middleware' => ['role:customer|create'], 'as' => 'editor.customer.create', 'uses' => 'CustomerController@create']);
	Route::post('/customer/create', ['middleware' => ['role:customer|create'], 'as' => 'editor.customer.store', 'uses' => 'CustomerController@store']);
		//edit
	Route::get('/customer/edit/{id}', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.edit', 'uses' => 'CustomerController@edit']);
	Route::put('/customer/edit/{id}', ['middleware' => ['role:customer|update'], 'as' => 'editor.customer.update', 'uses' => 'CustomerController@update']);
		//delete
	Route::delete('/customer/delete/{id}', ['middleware' => ['role:customer|delete'], 'as' => 'editor.customer.delete', 'uses' => 'CustomerController@delete']);
	Route::post('/customer/deletebulk', ['middleware' => ['role:customer|delete'], 'as' => 'editor.customer.deletebulk', 'uses' => 'CustomerController@deletebulk']);


	//Pricelist
		//index
	Route::get('/pricelist', ['middleware' => ['role:pricelist|read'], 'as' => 'editor.pricelist.index', 'uses' => 'PricelistController@index']);
	Route::get('/pricelist/data', ['as' => 'editor.pricelist.data', 'uses' => 'PricelistController@data']);
		//create
	Route::get('/pricelist/create', ['middleware' => ['role:pricelist|create'], 'as' => 'editor.pricelist.create', 'uses' => 'PricelistController@create']);
	Route::post('/pricelist/create', ['middleware' => ['role:pricelist|create'], 'as' => 'editor.pricelist.store', 'uses' => 'PricelistController@store']);
		//edit
	Route::get('/pricelist/edit/{id}', ['middleware' => ['role:pricelist|update'], 'as' => 'editor.pricelist.edit', 'uses' => 'PricelistController@edit']);
	Route::put('/pricelist/edit/{id}', ['middleware' => ['role:pricelist|update'], 'as' => 'editor.pricelist.update', 'uses' => 'PricelistController@update']);
		//delete
	Route::delete('/pricelist/delete/{id}', ['middleware' => ['role:pricelist|delete'], 'as' => 'editor.pricelist.delete', 'uses' => 'PricelistController@delete']);
	Route::post('/pricelist/deletebulk', ['middleware' => ['role:pricelist|delete'], 'as' => 'editor.pricelist.deletebulk', 'uses' => 'PricelistController@deletebulk']);


	//Item
		//index
	Route::get('/item', ['middleware' => ['role:item|read'], 'as' => 'editor.item.index', 'uses' => 'ItemController@index']);
	Route::get('/item/data', ['as' => 'editor.item.data', 'uses' => 'ItemController@data']);
		//create
	Route::get('/item/create', ['middleware' => ['role:item|create'], 'as' => 'editor.item.create', 'uses' => 'ItemController@create']);
	Route::post('/item/create', ['middleware' => ['role:item|create'], 'as' => 'editor.item.store', 'uses' => 'ItemController@store']);
		//edit
	Route::get('/item/edit/{id}', ['middleware' => ['role:item|update'], 'as' => 'editor.item.edit', 'uses' => 'ItemController@edit']);
	Route::put('/item/edit/{id}', ['middleware' => ['role:item|update'], 'as' => 'editor.item.update', 'uses' => 'ItemController@update']);
		//delete
	Route::delete('/item/delete/{id}', ['middleware' => ['role:item|delete'], 'as' => 'editor.item.delete', 'uses' => 'ItemController@delete']);
	Route::post('/item/deletebulk', ['middleware' => ['role:item|delete'], 'as' => 'editor.item.deletebulk', 'uses' => 'ItemController@deletebulk']);

	//Itembrand
		//index
	Route::get('/itembrand', ['middleware' => ['role:itembrand|read'], 'as' => 'editor.itembrand.index', 'uses' => 'ItembrandController@index']);
	Route::get('/itembrand/data', ['as' => 'editor.itembrand.data', 'uses' => 'ItembrandController@data']);
		//create
	Route::get('/itembrand/create', ['middleware' => ['role:itembrand|create'], 'as' => 'editor.itembrand.create', 'uses' => 'ItembrandController@create']);
	Route::post('/itembrand/create', ['middleware' => ['role:itembrand|create'], 'as' => 'editor.itembrand.store', 'uses' => 'ItembrandController@store']);
		//edit
	Route::get('/itembrand/edit/{id}', ['middleware' => ['role:itembrand|update'], 'as' => 'editor.itembrand.edit', 'uses' => 'ItembrandController@edit']);
	Route::put('/itembrand/edit/{id}', ['middleware' => ['role:itembrand|update'], 'as' => 'editor.itembrand.update', 'uses' => 'ItembrandController@update']);
		//delete
	Route::delete('/itembrand/delete/{id}', ['middleware' => ['role:itembrand|delete'], 'as' => 'editor.itembrand.delete', 'uses' => 'ItembrandController@delete']);
	Route::post('/itembrand/deletebulk', ['middleware' => ['role:itembrand|delete'], 'as' => 'editor.itembrand.deletebulk', 'uses' => 'ItembrandController@deletebulk']);

	//Itemcategory
		//index
	Route::get('/itemcategory', ['middleware' => ['role:itemcategory|read'], 'as' => 'editor.itemcategory.index', 'uses' => 'ItemcategoryController@index']);
	Route::get('/itemcategory/data', ['as' => 'editor.itemcategory.data', 'uses' => 'ItemcategoryController@data']);
		//create
	Route::get('/itemcategory/create', ['middleware' => ['role:itemcategory|create'], 'as' => 'editor.itemcategory.create', 'uses' => 'ItemcategoryController@create']);
	Route::post('/itemcategory/create', ['middleware' => ['role:itemcategory|create'], 'as' => 'editor.itemcategory.store', 'uses' => 'ItemcategoryController@store']);
		//edit
	Route::get('/itemcategory/edit/{id}', ['middleware' => ['role:itemcategory|update'], 'as' => 'editor.itemcategory.edit', 'uses' => 'ItemcategoryController@edit']);
	Route::put('/itemcategory/edit/{id}', ['middleware' => ['role:itemcategory|update'], 'as' => 'editor.itemcategory.update', 'uses' => 'ItemcategoryController@update']);
		//delete
	Route::delete('/itemcategory/delete/{id}', ['middleware' => ['role:itemcategory|delete'], 'as' => 'editor.itemcategory.delete', 'uses' => 'ItemcategoryController@delete']);
	Route::post('/itemcategory/deletebulk', ['middleware' => ['role:itemcategory|delete'], 'as' => 'editor.itemcategory.deletebulk', 'uses' => 'ItemcategoryController@deletebulk']);


	 //Item Unit
		//index
	Route::get('/itemunit', ['middleware' => ['role:itemunit|read'], 'as' => 'editor.itemunit.index', 'uses' => 'ItemunitController@index']);
	Route::get('/itemunit/data', ['as' => 'editor.itemunit.data', 'uses' => 'ItemunitController@data']);
		//create
	Route::get('/itemunit/create', ['middleware' => ['role:itemunit|create'], 'as' => 'editor.itemunit.create', 'uses' => 'ItemunitController@create']);
	Route::post('/itemunit/create', ['middleware' => ['role:itemunit|create'], 'as' => 'editor.itemunit.store', 'uses' => 'ItemunitController@store']);
		//edit
	Route::get('/itemunit/edit/{id}', ['middleware' => ['role:itemunit|update'], 'as' => 'editor.itemunit.edit', 'uses' => 'ItemunitController@edit']);
	Route::put('/itemunit/edit/{id}', ['middleware' => ['role:itemunit|update'], 'as' => 'editor.itemunit.update', 'uses' => 'ItemunitController@update']);
		//delete
	Route::delete('/itemunit/delete/{id}', ['middleware' => ['role:itemunit|delete'], 'as' => 'editor.itemunit.delete', 'uses' => 'ItemunitController@delete']);
	Route::post('/itemunit/deletebulk', ['middleware' => ['role:itemunit|delete'], 'as' => 'editor.itemunit.deletebulk', 'uses' => 'ItemunitController@deletebulk']);

	//Userfilter
		//index
	Route::get('/userfilter', ['middleware' => ['role:userfilter|read'], 'as' => 'editor.userfilter.index', 'uses' => 'UserfilterController@index']);
	Route::get('/userfilter/data', ['as' => 'editor.userfilter.data', 'uses' => 'UserfilterController@data']);
		//create
	Route::get('/userfilter/create', ['middleware' => ['role:userfilter|create'], 'as' => 'editor.userfilter.create', 'uses' => 'UserfilterController@create']);
	Route::post('/userfilter/create', ['middleware' => ['role:userfilter|create'], 'as' => 'editor.userfilter.store', 'uses' => 'UserfilterController@store']);
		//edit
	Route::get('/userfilter/edit/{id}', ['middleware' => ['role:userfilter|update'], 'as' => 'editor.userfilter.edit', 'uses' => 'UserfilterController@edit']);
	Route::put('/userfilter/edit/{id}', ['middleware' => ['role:userfilter|update'], 'as' => 'editor.userfilter.update', 'uses' => 'UserfilterController@update']);
		//delete
	Route::delete('/userfilter/delete/{id}', ['middleware' => ['role:userfilter|delete'], 'as' => 'editor.userfilter.delete', 'uses' => 'UserfilterController@delete']);
	Route::post('/userfilter/deletebulk', ['middleware' => ['role:userfilter|delete'], 'as' => 'editor.userfilter.deletebulk', 'uses' => 'UserfilterController@deletebulk']);

	//Faq
		//index
	Route::get('/faq', ['middleware' => ['role:faq|read'], 'as' => 'editor.faq.index', 'uses' => 'FaqController@index']);
	Route::get('/faq/data', ['as' => 'editor.faq.data', 'uses' => 'FaqController@data']);
		//create
	Route::get('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'editor.faq.create', 'uses' => 'FaqController@create']);
	Route::post('/faq/create', ['middleware' => ['role:faq|create'], 'as' => 'editor.faq.store', 'uses' => 'FaqController@store']);
		//edit
	Route::get('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'editor.faq.edit', 'uses' => 'FaqController@edit']);
	Route::put('/faq/edit/{id}', ['middleware' => ['role:faq|update'], 'as' => 'editor.faq.update', 'uses' => 'FaqController@update']);
		//delete
	Route::delete('/faq/delete/{id}', ['middleware' => ['role:faq|delete'], 'as' => 'editor.faq.delete', 'uses' => 'FaqController@delete']);
	Route::post('/faq/deletebulk', ['middleware' => ['role:faq|delete'], 'as' => 'editor.faq.deletebulk', 'uses' => 'FaqController@deletebulk']);


	//Popup
		//index
	Route::get('/popup', ['middleware' => ['role:popup|read'], 'as' => 'editor.popup.index', 'uses' => 'PopupController@index']);
		//edit
	Route::get('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.edit', 'uses' => 'PopupController@edit']);
	Route::put('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.update', 'uses' => 'PopupController@update']);
});

//api
Route::group(['prefix' => 'api', 'middleware' => 'auth', 'namespace' => 'Editor'], function(){
	Route::get('/user', ['middleware' => ['role:userfilter|read'], 'as' => 'api.user.data', 'uses' => 'UserfilterController@dataUser']);
});
