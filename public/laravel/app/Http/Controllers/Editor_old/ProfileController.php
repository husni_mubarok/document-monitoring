<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Hash;
use File;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\DB;
use View;
use App\Model\Branch;
use Carbon\Carbon;


class ProfileController extends Controller
{
    //
    protected $UserRepository;

    public function __construct(UserRepository $user_repository)
    {
    	$this->middleware(function ($request, $next) {
            $userid = auth()->id();
            // return $next($request);
            // dd($userid);
            $sql_branch_filter = 'SELECT
                        user_branch.user_id,
                        branch.id,
                        branch.branch_name,
                        branch.description,
                        branch.field_name 
                      FROM
                        branch
                      LEFT JOIN user_branch ON branch.id = user_branch.branch_id 
                      INNER JOIN user ON user_branch.user_id = user.id 
                      WHERE `user`.id = '.$userid.'';
            $this->branch_global = DB::table(DB::raw("($sql_branch_filter) as rs_sql"))->get(); 
            View::share([ 'branch_global' => $this->branch_global ]); 
        return $next($request);
      });
        

        $branch = Branch::all();
        $sql = 'SELECT
                    "EMKL Cost" AS type,
                    "" AS po_number,
                    "" AS container_no,
                    "" AS po_date,
                    "" AS tarik,
                    "" AS currency_name,
                    "" AS currency_name_to,
                    "" AS totalmodal,
                    "" AS totalmodal_to,
                    emkl_cost_log.id,
                    DATE_FORMAT(emkl_cost_log.datefrom, "%d-%m-%Y") AS datefrom,
                    DATE_FORMAT(emkl_cost_log.dateto, "%d-%m-%Y") AS dateto,
                    FORMAT(emkl_cost_log.emkl, 0) AS emkl,
                    FORMAT(emkl_cost_log.`plugin`, 0) AS plugin,
                    FORMAT(emkl_cost_log.ls, 0) AS ls,
                    FORMAT(emkl_cost_log.kalog, 0) AS kalog,
                    DATE_FORMAT(emkl_cost_log.datefrom_to, "%d-%m-%Y") AS datefrom_to,
                    DATE_FORMAT(emkl_cost_log.dateto_to, "%d-%m-%Y") AS dateto_to,
                    FORMAT(emkl_cost_log.emkl_to, 0) AS emkl_to,
                    FORMAT(emkl_cost_log.`plugin_to`, 0) AS plugin_to,
                    FORMAT(emkl_cost_log.ls_to, 0) AS ls_to,
                    FORMAT(emkl_cost_log.kalog_to, 0) AS kalog_to,
                    `user`.username,
                    `user`.read_notif,
                    emkl_cost_log.updated_at AS updated_at_short, ';
                   foreach ($branch as $key => $branchs) {
                      $sql .= "NULL AS " . $branchs->field_name . ", ";
                      $sql .= "NULL AS " . $branchs->field_name . "_to, ";
                    };
                  $sql .= ' DATE_FORMAT(emkl_cost_log.updated_at, "%d-%m-%Y %H:%i") AS updated_at FROM
                  emkl_cost_log
                  JOIN `user`
                  ON emkl_cost_log.updated_by = `user`.id  

                 UNION ALL

                 SELECT
                    "Branch Allocation" AS type,
                    purchase_order.po_number,
                    container.container_no,
                    DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS po_date,
                    purchase_order_detail_log.tarik AS tarik,
                    currency.currency_name,
                    currency_to.currency_name AS currency_name_to,
                    purchase_order_detail_log.totalmodal,
                    purchase_order_detail_log.totalmodal_to,
                    purchase_order_detail_log.id,
                    "" AS datefrom,
                    "" AS dateto,
                    "" AS emkl,
                    "" AS plugin,
                    "" AS ls,
                    "" AS kalog,
                    "" AS datefrom_to,
                    "" AS dateto_to,
                    "" AS emkl_to,
                    "" AS plugin_to,
                    "" AS ls_to,
                    "" AS kalog_to, 
                    `user`.username,
                    `user`.read_notif,
                    purchase_order_detail_log.updated_at AS updated_at_short, ';
                   foreach ($branch as $key => $branchs) {
                      $sql .= "IFNULL(purchase_order_detail_log." . $branchs->field_name . ",0) AS " . $branchs->field_name . ", ";
                      $sql .= "IFNULL(purchase_order_detail_log." . $branchs->field_name . "_to,0) AS " . $branchs->field_name . "_to, ";
                    };
                      $sql .= ' DATE_FORMAT(purchase_order_detail_log.updated_at, "%d-%m-%Y %H:%i") AS updated_at FROM
                          purchase_order_detail_log INNER JOIN purchase_order_detail ON purchase_order_detail_log.purchase_order_detail_id = purchase_order_detail.id
                          INNER JOIN purchase_order ON purchase_order_detail.purchase_order_id = purchase_order.id
                          LEFT JOIN container ON purchase_order.container_id = container.id
                          LEFT JOIN currency ON purchase_order_detail_log.currency_id = currency.id
                          LEFT JOIN currency AS currency_to ON purchase_order_detail_log.currency_id_to = currency_to.id
                          JOIN `user`
                      ON purchase_order_detail_log.updated_by = `user`.id';

        $this->notif = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('updated_at_short','DESC')->get(); 

        View::share([ 'notif' => $this->notif ]); 

        // $this->branch_global = Branch::all();


        $this->notif_limit = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('updated_at_short','DESC')->limit(5)->get(); 

        View::share([ 'notif_limit' => $this->notif_limit ]); 
    }

    public function show()
    {
    	return view ('editor.profile.detail');
    }

    public function delete_image()
    {
        $this->UserRepository->delete_image(Auth::user()->id);
        return redirect()->action('Editor\ProfileController@show');
    }

    public function edit()
    {
    	return view ('editor.profile.edit');
    }

    public function update(Request $request)
    {
    	$data = array(
            'email' => $request->input('email'), 
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'), 
            );

        $rules = [
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => '',
        ];

        if($request->image)
        {
            $data['image'] = $request->image;
            $rules['image'] = 'image|between:0, 5000';
        }

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {   
            return redirect()->action('Editor\ProfileController@edit')->withInput()->withErrors($validator);
        } else {
            // dd($request->input());
            // $this->UserRepository->update(Auth::user()->id, $request->input());

            $user = User::where('id', Auth::user()->id)->first();
            $user->email = $request->input('email');
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');            
     
            $user->save();

            if($request->image)
            {
                // $this->UserRepository->update_image(Auth::user()->id, $request->image);
                $user = User::FindOrFail(Auth::user()->id);

                $original_directory = "uploads/user/".$user->username."/";
                
                if(!File::exists($original_directory))
                {
                    File::makeDirectory($original_directory, $mode = 0777, true, true);
                }
                $file_extension = $request->image->getClientOriginalExtension();
                $user->filename = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
                $request->image->move($original_directory, $user->filename);

                $thumbnail_directory = $original_directory."thumbnail/";
                if(!File::exists($thumbnail_directory))
                {
                    File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
                }
                $thumbnail = Image::make($original_directory.$user->filename);
                $thumbnail->fit(300,300)->save($thumbnail_directory.$user->filename);

                $user->save();
            }
 
            return redirect()->action('Editor\ProfileController@show');
        }
    }

    public function edit_password()
    {
        return view ('editor.profile.password');
    }

    public function update_password(Request $request)
    {
        $data = array(
            'password_current' => $request->input('password_current'), 
            'password_new' => $request->input('password_new'),
            'password_new_confirmation' => $request->input('password_new_confirmation'), 
            );

        $rules = [
            'password_current' => 'required',
            'password_new' => 'required|confirmed',
            'password_new_confirmation' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {   
            return redirect()->action('Editor\ProfileController@edit_password')->withInput()->withErrors(['New password confirmation failed!']);
        } else {
            
            $user = $this->UserRepository->get_one(Auth::user()->id);
            
            if(Hash::check($request->input('password_current'), $user->password))
            {
                $this->UserRepository->change_password(Auth::user()->id, $request->input('password_new'));

                return redirect()->action('Editor\ProfileController@show');
            } else {
                return redirect()->action('Editor\ProfileController@edit_password')->withInput()->withErrors(['Current password mismatch!']);
            }
        }
    }
}
