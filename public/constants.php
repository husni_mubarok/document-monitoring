<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => 'http://puprovjambi.id/laravel_einfo/uploads',
        'bootstrap' => 'http://puprovjambi.id/laravel_einfo/laravel/bootstrap',
        'css' => 'http://puprovjambi.id/laravel_einfo/assets/css',
        'img' => 'http://puprovjambi.id/laravel_einfo/assets/img',
        'js' => 'http://puprovjambi.id/laravel_einfo/assets/js',
        'plugin' => 'http://puprovjambi.id/laravel_einfo/assets/plugins',
    ],

];